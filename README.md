Terraform Jsonnet Grafonnet Module
==================================

Distribution of [grafonnet library](https://github.com/grafana/grafonnet/)
as Terraform module. Intended to use with the
[Jsonnet provider](https://github.com/alxrem/terraform-provider-jsonnet).

Module only contains the library code and outputs absolute path to it.
[Version of module](https://gitlab.com/tf-org-ru/terraform-jsonnet-grafonnet/-/infrastructure_registry)
corresponds to the library version.

Terraform module distributive doesn't support symlinks, so symlinks from
`vendor/...` to `vendor/github.com/grafana/grafonnet/gen/...` are replaced with
the directories containing copies of corresponding `main.libsonnet` to keep
partial ability to use shortened import paths, for example

```jsonnet
local g = import 'grafonnet-latest/main.libsonnet';
```

instead of

```jsonnet
local g = import 'github.com/grafana/grafonnet/gen/grafonnet-latest/main.libsonnet';
```

## Usage example

```terraform
module "grafonnet" {
  source  = "gitlab.com/tf-org-ru/grafonnet/jsonnet"
  version = "10.4.0"
}

data "grafana_data_source" "default" {
  name = "grafana-cloud-prom"
}

data "jsonnet_file" "dashboard" {
  jsonnet_path = module.grafonnet.jsonnet_path

  source = "${path.module}/dashboards/node.jsonnet"

  ext_str = {
    "datasource" = data.grafana_data_source.default.uid
  }
}

resource "grafana_dashboard" "node" {
  config_json = data.jsonnet_file.dashboard.rendered
}
```

<!-- BEGIN_TF_DOCS -->
## Outputs

| Name | Description |
|------|-------------|
| <a name="output_jsonnet_path"></a> [jsonnet\_path](#output\_jsonnet\_path) | Absolute path to grafonnet bundle. |
<!-- END_TF_DOCS -->