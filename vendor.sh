#!/bin/sh

REF=${1:-main}

rm -rf jsonnetfile* vendor/
jb init
jb install github.com/grafana/grafonnet/gen/grafonnet-latest@$REF

find vendor/ -maxdepth 1 -type l | while read SYMLINK; do
	TMP_MAIN=`mktemp`;
	cat $SYMLINK/main.libsonnet > $TMP_MAIN;
	rm -f $SYMLINK;
	mkdir $SYMLINK;
	mv $TMP_MAIN $SYMLINK/main.libsonnet;
done

git add jsonnetfile* vendor/