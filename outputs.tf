output "jsonnet_path" {
  description = "Absolute path to grafonnet bundle."

  value = local.jsonnet_path
}